import itertools
import math
import time

import torch
import torchvision
import torch.nn as nn
import torchvision.datasets as dsets
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
from torch.autograd import Variable

from IPython import display

from models.one import Discriminator, Generator

NUM_TEST_SAMPLES = 16
CONSTANT_NOISE = Variable(torch.randn(NUM_TEST_SAMPLES, 64)).cuda()

def train_discriminator(discriminator, criterion, optimizer, real_images, real_labels, fake_images, fake_labels):
    discriminator.zero_grad()

    # Train on the real images first...
    real_outputs = discriminator(real_images)
    real_loss    = criterion(real_outputs, real_labels)

    # ... then do the fake images.
    fake_outputs = discriminator(fake_images)
    fake_loss    = criterion(fake_outputs, fake_labels)

    total_loss = real_loss + fake_loss
    total_loss.backward()
    optimizer.step()

    return total_loss, real_outputs, fake_outputs

def train_generator(generator, criterion, optimizer, discriminator_outputs, real_labels):
    generator.zero_grad()

    loss = criterion(discriminator_outputs, real_labels)
    loss.backward()
    optimizer.step()

    return loss

def save_and_show_results(generator, path, epoch, ax):
    images = generator(CONSTANT_NOISE)
    print(images)

    for k in range(NUM_TEST_SAMPLES):
        i = k // 4
        j = k %  4
        ax[i,j].cla()
        ax[i,j].imshow(images[k,:].data.cpu().numpy().reshape(28,28), cmap='Greys')
    display.clear_output(wait=True)
    display.display(plt.gcf())

    file_name = f'mnist-gan-{epoch:03d}'
    plt.savefig(f'{path}/{file_name}')

def train_for_epoch(train_loader, generator, generator_optimizer, discriminator, discriminator_optimizer, criterion, epoch, ax):
    for n, (images, _) in enumerate(train_loader):
        real_images = Variable(images).cuda()
        real_labels = Variable(torch.ones(real_images.size(0))).cuda()

        noise = Variable(torch.randn(real_images.size(0), 64)).cuda()
        fake_images = generator(noise)
        fake_labels = Variable(torch.zeros(real_images.size(0))).cuda()

        discriminator_loss, real_score, fake_score = train_discriminator(discriminator, criterion, discriminator_optimizer, real_images, real_labels, fake_images, fake_labels)

        new_noise = Variable(torch.randn(real_images.size(0), 64)).cuda()
        fake_images = generator(new_noise)
        outputs = discriminator(fake_images)

        generator_loss = train_generator(generator, criterion, generator_optimizer, outputs, real_labels)

    save_and_show_results(generator, 'results/', epoch, ax)

    print(f'Epoch: {epoch:03d}    D_loss: {discriminator_loss:0.4f}   G_loss: {generator_loss:0.4f}   D(x): {real_score.data.mean():0.2f}   D(G(z)): {fake_score.data.mean():0.2f}')

if __name__ == '__main__':
    generator = Generator().cuda()
    discriminator = Discriminator().cuda()

    criterion = nn.BCELoss()
    lr = 0.0002
    discriminator_optimizer = torch.optim.Adam(discriminator.parameters(), lr=lr)
    generator_optimizer = torch.optim.Adam(generator.parameters(), lr=lr)

    transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))
    ])

    train_dataset = dsets.MNIST(root='./data/', train=True, download=True, transform=transform)
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=64, shuffle=True)

    size_figure_grid = int(math.sqrt(NUM_TEST_SAMPLES))
    fig, ax = plt.subplots(size_figure_grid, size_figure_grid, figsize=(6, 6))
    for i, j in itertools.product(range(size_figure_grid), range(size_figure_grid)):
        ax[i,j].get_xaxis().set_visible(False)
        ax[i,j].get_yaxis().set_visible(False)

    epochs = range(200)
    for epoch in epochs:
        train_for_epoch(train_loader, generator, generator_optimizer, discriminator, discriminator_optimizer, criterion, epoch, ax)

