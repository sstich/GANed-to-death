# README.md

Re-implementation of GANs on MNIST, with increasing degrees of sophistication — starting with a vanilla, fully connected GAN and then adding more and more improvements (e.g., changing the architecture, being cleverer with training strategies, etc.)

Thanks to [prcastro](https://github.com/prcastro/pytorch-gan) for the first architecture, to [znxlwm](https://github.com/znxlwm/tensorflow-MNIST-GAN-DCGAN) for the DCGAN, and to [junyanz](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/models/cycle_gan_model.py) for his pix2pix and CycleGAN implementations.
